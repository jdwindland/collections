

import org.hibernate.*;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class MemberDAO {

    //Creates a session factory.
    private static SessionFactory ourSessionFactory = null;

    static {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            ourSessionFactory = configuration.buildSessionFactory();
        } catch (HibernateException e) {
            System.out.println("\nUnable to create a session factory.\n");
            e.printStackTrace();
        }
    }

    //Method to access database and return list of pets.
    public static void listMembers() {
        Transaction t = null;
        try (Session session = ourSessionFactory.openSession()) {
            t = session.beginTransaction();
            List members = session.createQuery("From Member").list();
            System.out.println("\n------------------------------------------");
            System.out.println("ID\t\tName\t\tPreferred Name\tAddress 1\t\tAddress 2\t\tAddress 3" +
                    "\t\tPhone\t\tBirthday\t\tMonth\t\tAnniversary");
            System.out.println("------------------------------------------");
            for (Object o : members) {
                Member m = (Member) o;
                System.out.print(m.getId() + "\t\t" + m.getName() +
                        "\t\t" + m.getPname() + "\t\t" + m.getAddr1() +
                        "\t\t" + m.getAddr2() + "\t\t" + m.getAddr3() +
                        "\t\t" + m.getPhone() + "\t\t" + m.getBday() +
                        "\t\t" + m.getBdaymo() + "\t\t" + m.getAnniversary() + "\n");
            }
            t.commit();
        } catch (HibernateException e) {
            if (t != null) t.rollback();
            System.out.print("\nUnable to list pets.\n");
            e.printStackTrace();
        }
    }
}