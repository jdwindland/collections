
import javax.persistence.*;

@Entity
@Table(name = "family_members")
public class Member {

    //Auto generates ID value.
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    //Maps each attribute to the column in tbl_pets table in database.
    @Column(name = "ID")
    private int id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Preferred")
    private String pname;
    @Column(name = "Addr1")
    private String addr1;
    @Column(name = "Addr2")
    private String addr2;
    @Column(name = "Addr3")
    private String addr3;
    @Column(name = "Phone")
    private String phone;
    @Column(name = "Birthday")
    private String bday;
    @Column(name = "Month")
    private String bdaymo;
    @Column(name = "Anniversary")
    private String anniversary;

    public Member(){}

    public Member(String name, String pname, String addr1, String addr2, String addr3,
                  String phone, String bday, String bdaymo, String anniversary){
        super();
        this.name = name;
        this.pname = pname;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.addr3 = addr3;
        this.phone = phone;
        this.bday = bday;
        this.bdaymo = bdaymo;
        this.anniversary = anniversary;
    }

    public Member(int id, String name, String pname, String addr1, String addr2, String addr3,
                  String phone, String bday, String bdaymo, String anniversary){
        super();
        this.id = id;
        this.name = name;
        this.pname = pname;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.addr3 = addr3;
        this.phone = phone;
        this.bday = bday;
        this.bdaymo = bdaymo;
        this.anniversary = anniversary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getAddr1() {
        return addr1;
    }

    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    public String getAddr2() {
        return addr2;
    }

    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    public String getAddr3() {
        return addr3;
    }

    public void setAddr3(String addr3) {
        this.addr3 = addr3;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBday() {
        return bday;
    }

    public void setBday(String bday) {
        this.bday = bday;
    }

    public String getBdaymo() {
        return bdaymo;
    }

    public void setBdaymo(String bdaymo) {
        this.bdaymo = bdaymo;
    }

    public String getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }
}

