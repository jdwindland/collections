import java.util.Comparator;

public class GroceryComp implements Comparator<Grocery>{

    public int compare(Grocery g1, Grocery g2) {
        return g1.getItem().compareTo(g2.getItem());
    }
}