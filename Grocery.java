
public class Grocery {

    public String item;
    public int qty;

    public Grocery(String item, int qty) {
        this.item = item;
        this.qty = qty;
    }

    public String getItem() {
        return item;
    }

    public String toString() {
        return "Item: " + item + "     Qty: " + qty;
    }

}