import java.util.*;

public class Collections {

    public static void main(String[] args) {

        //COLLECTION: List - allows duplicates

        // Create Jenny's grocery list - eggs listed twice
            List<String> list1 = new ArrayList<>();

        //Create Joseph's grocery list - duplicates 3 items on Jenny's list (eggs, milk, bread)
            List<String> list2 = new ArrayList<>();
                    
        //Add items to both lists
        try {
            list1.add("Milk");
            list1.add("Bread");
            list1.add("Eggs");
            list1.add("Orange Juice");
            list1.add("Chicken");
            list1.add("Chobani Flips Yougurt");
            list1.add("Fugi Apples");
            list1.add("V8 Energy");
            list1.add("Powerful Drinks");
            list1.add("Eggs");
            list2.add("NOS");
            list2.add("V8 Low Salt");
            list2.add("Milk");
            list2.add("Rice Pudding");
            list2.add("Eggs");
            list2.add("Protein Snack Pack");
            list2.add("Almonds");
            list2.add("Ice Cream");
            list2.add("Chilli");
            list2.add("Chicken Strips");
        }
        catch (Exception e1) {
            System.out.println("There is an exception with adding an item to the list.");
        }

        //Print Jenny's grocery list
        System.out.println("--------------------");
        System.out.println("Jenny's Grocery List");
        System.out.println("--------------------");
        for (Object jenlist : list1) {
            System.out.println((String) jenlist);
        }
        System.out.println("");

        //Print Joseph's grocery list
        System.out.println("--------------------");
        System.out.println("Joseph's Grocery List");
        System.out.println("--------------------");
        for (Object joelist : list2) {
            System.out.println((String) joelist);
        }
        System.out.println("");


        //COLLECTION: Set - does not allow duplicates

        //Create a new list that combines Jenny's and Joseph's grocery lists with duplicates removed
        //Create list3 by copying list1 into a set and then adding all the items from list2
        Set<String> list3 = new HashSet<>(list1);
        list3.addAll(list2);

        //Print Combined grocery list
        System.out.println("--------------------");
        System.out.println("Combined Grocery List");
        System.out.println("--------------------");
        for (Object comlist : list3) {
            System.out.println((String) comlist);
        }
        System.out.println("");


        //COLLECTION: Tree - Uses Generics and Comparator to sort the list

        //Create Tree
        TreeSet<Grocery> list4 = new TreeSet<Grocery>(new GroceryComp());

        //Add items to list
        try {
            list4.add(new Grocery("Milk", 1));
            list4.add(new Grocery("Bread", 1));
            list4.add(new Grocery("Eggs", 12));
            list4.add(new Grocery("Orange Juice", 2));
            list4.add(new Grocery("Chicken", 1));
            list4.add(new Grocery("Chobani Flips Yougurt", 7));
            list4.add(new Grocery("Fugi Apples", 4));
            list4.add(new Grocery("V8 Energy", 12));
            list4.add(new Grocery("Powerful Drinks", 6));
            list4.add(new Grocery("NOS", 1));
            list4.add(new Grocery("V8 Low Salt", 2));
            list4.add(new Grocery("Rice Pudding", 6));
            list4.add(new Grocery("Protein Snack Pack", 5));
            list4.add(new Grocery("Almonds", 1));
            list4.add(new Grocery("Ice Cream", 2));
            list4.add(new Grocery("Chilli", 4));
            list4.add(new Grocery("Chicken Strips", 1));
        }
        catch(Exception e2) {
            System.out.println("There is an issue adding an item to the list. Make sure you are using a grocery object type.");
        }

        //Print grocery list
        System.out.println("--------------------");
        System.out.println("Final Grocery List");
        System.out.println("--------------------");
        for (Grocery finlist : list4) {
            System.out.println(finlist);
        }
        System.out.println("");


        //COLLECTION: Queue - Linked List orders FIFO while Priority Queue will order list alphabetically
        
        //Create queue for grocery stores
        Queue<String> store1 = new LinkedList<String>();
        Queue<String> store2 = new PriorityQueue<String>();

        //Add stores to the Queue
        try {
            store1.add("WalMart");
            store1.add("IGA");
            store1.add("Krogers");
            store1.add("Costco");
            store1.add("HyVee");
            store2.add("WalMart");
            store2.add("IGA");
            store2.add("Krogers");
            store2.add("Costco");
            store2.add("HyVee");
        }
        catch (Exception e3) {
            System.out.println("There is an exception with adding an item to the list.");
        }

        //Print List of Stores
        System.out.println("---------------------------");
        System.out.println("Grocery Stores - LinkedList");
        System.out.println("---------------------------");
        Iterator iterator = store1.iterator();
        while (iterator.hasNext()) {
            System.out.println(store1.poll());
        }
        System.out.println("");

        System.out.println("------------------------------");
        System.out.println("Grocery Stores - PriorityQueue");
        System.out.println("------------------------------");
        Iterator iterator2 = store2.iterator();
        while (iterator2.hasNext()) {
            System.out.println(store2.poll());
        }
        System.out.println("");
    }
}